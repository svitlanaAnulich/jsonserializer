package com.json.mapper;

import com.json.writer.JsonWriter;

/**
 * The class writes {@link Character} object
 */
public class CharMapper implements Mapper<Character> {


    /**
     * Method writes {@link Character} object, for write uses classes that implement {@link JsonWriter}
     * @param character - char for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Character character, JsonWriter jsonWriter) {
        jsonWriter.writeChar(character);
    }
}
