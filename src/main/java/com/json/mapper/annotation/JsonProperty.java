package com.json.mapper.annotation;

import java.lang.annotation.*;

/**
 * An annotation that indicates this member should be serialized to JSON regardless of its modifiers.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonProperty {
}
