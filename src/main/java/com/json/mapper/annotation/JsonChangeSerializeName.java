package com.json.mapper.annotation;

import java.lang.annotation.*;

/**
 * An annotation that indicates this member should be serialized to JSON with
 * the provided name value as its field name.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonChangeSerializeName {

    /**
     * @return the desired name of the field when it is serialized
     */
    String name();
}
