package com.json.mapper.annotation;

import java.lang.annotation.*;

/**
 * An annotation that indicates this member should be ignored in serialization to JSON.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonIgnore {
}
