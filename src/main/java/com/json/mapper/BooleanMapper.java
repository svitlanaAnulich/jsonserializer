package com.json.mapper;

import com.json.writer.JsonWriter;

/**
 * The class writes {@link Boolean} object
 */
public class BooleanMapper implements Mapper<Boolean> {

    /**
     * Method writes {@link Boolean} object, for write uses classes that implement {@link JsonWriter}
     * @param aBoolean - boolean for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Boolean aBoolean, JsonWriter jsonWriter) {
       jsonWriter.writeBoolean(aBoolean);
    }
}
