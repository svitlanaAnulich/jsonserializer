package com.json.mapper;

import com.json.mapper.annotation.JsonChangeSerializeName;
import com.json.mapper.annotation.JsonIgnore;
import com.json.mapper.annotation.JsonProperty;
import com.json.serializer.IndentedJsonSerializer;
import com.json.writer.JsonWriter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

/**
 * Used for writing objects of different type using {@link java.lang.reflect}
 */
public class ObjectMapper implements Mapper {

    private IndentedJsonSerializer serializer;

    /**
     * Constructor initialize parameter {@link #serializer}
     * @param serializer - defines way to serialize child objects.
     */
    public ObjectMapper(IndentedJsonSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * By default writes public, protected and fields marked with {@link JsonProperty}.
     * Ignores private, transient and fields with any modifier, annotations marked with {@link JsonIgnore}.
     * Also writes corresponding fields from super classes of given object.
     * Change field name if present {@link JsonChangeSerializeName}
     * @param object - object for writing to writer
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Object object, JsonWriter jsonWriter) {
        jsonWriter.writeObjectBegin();
        List<Field> fields = getFieldForJson(getFieldsIncludingSuper(object));

        Iterator<Field> iterator = fields.iterator();
        while (iterator.hasNext()) {
            Field field = iterator.next();
            field.setAccessible(true);
            if (field.isAnnotationPresent(JsonChangeSerializeName.class)) {
                jsonWriter.writeString(field.getAnnotation(JsonChangeSerializeName.class).name());
            } else {
                jsonWriter.writeString(field.getName());
            }
            jsonWriter.writePropertySeparator();
            try {
                serializer.serialize(field.get(object), jsonWriter);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            if (iterator.hasNext()) jsonWriter.writeSeparator();
        }
        jsonWriter.writeObjectEnd();
    }

    /**
     * Create list with all fields from class and it's supers classes
     * @param object - object for starting collects
     * @return - fields list
     */
    private List<Field> getFieldsIncludingSuper(Object object) {
        List<Field> fields = new ArrayList<>();
        Class<?> currentClass = object.getClass();
        while (currentClass != Object.class) {
            fields.addAll(
                    removeFieldsWithEqualsName(
                            new ArrayList<>(Arrays.asList(currentClass.getDeclaredFields())),
                            fields));
            currentClass = currentClass.getSuperclass();
        }
        return fields;
    }

    /**
     * Remove fields from fields list of supperclass if it exist at children class
     * fields equals by name
     * @param removeFieldList - fields list from superclass
     * @param fieldList - field list from children class
     * @return fields set
     */
    private List<Field> removeFieldsWithEqualsName(List<Field> removeFieldList,List<Field> fieldList) {
        for (int i = removeFieldList.size()-1; i >=0; i--) {
            for (Field field: fieldList) {
                if (removeFieldList.get(i).getName().equals(field.getName())) {
                    removeFieldList.remove(i);
                    break;
                }
            }
        }

        return removeFieldList;
    }

    /**
     * Create list with fields witch will be serialize to JSON
     * @param fieldList - list with all fields
     * @return - list with serializing fields
     */
    private List<Field> getFieldForJson(List<Field> fieldList) {
        List<Field> outList = new ArrayList<>();
        for (Field field : fieldList) {
            if (isSerializeField(field)) outList.add(field);
        }
        return outList;
    }

    /**
     * Check is field will be serialize
     * @param field - field which need checked
     * @return - true if field should be serialized and false in another case
     */
    private boolean isSerializeField(Field field) {
        if (field.isAnnotationPresent(JsonIgnore.class)) {
            return false;
        }
        if (field.isAnnotationPresent(JsonProperty.class)) {
            return true;
        }
        if (Modifier.isTransient(field.getModifiers())) {
            return false;
        }
        if (Modifier.isProtected(field.getModifiers())) {
            return true;
        }

        return Modifier.isPublic(field.getModifiers());

    }
}
