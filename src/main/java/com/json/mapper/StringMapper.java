package com.json.mapper;

import com.json.writer.JsonWriter;

/**
 * The class writes {@link String} to JSON.
 */
public class StringMapper implements Mapper<String> {

    /**
     * Method writes {@link String}, for write uses classes that implement {@link JsonWriter}
     * @param string - {@link String} for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(String string, JsonWriter jsonWriter) {
        jsonWriter.writeString(string);
    }
}
