package com.json.mapper;

import com.json.serializer.IndentedJsonSerializer;
import com.json.writer.JsonWriter;

import java.util.Iterator;
import java.util.Map;

/**
 * Writes instances of {@link Map} to JSON, where key and value are separated by colon.
 */
public class MapMapper implements Mapper<Map> {

    private IndentedJsonSerializer serializer;

    /**
     * Constructor contains parameter {@link #serializer}
     * @param serializer - defines way to serialize child objects
     */
    public MapMapper(IndentedJsonSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Method writes colon between key and value, for write uses classes that implement {@link JsonWriter}
     * @param object - {@link Map} for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Map object, JsonWriter jsonWriter) {
        jsonWriter.writeObjectBegin();
        Iterator<Map.Entry> iterator = object.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry entry = iterator.next();
            serializer.serialize(entry.getKey(), jsonWriter);
            jsonWriter.writePropertySeparator();
            serializer.serialize(entry.getValue(), jsonWriter);
            if (iterator.hasNext()) jsonWriter.writeSeparator();
        }
        jsonWriter.writeObjectEnd();
    }
}
