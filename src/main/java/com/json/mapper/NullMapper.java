package com.json.mapper;

import com.json.writer.JsonWriter;

/**
 * The class writes null to JSON.
 */
public class NullMapper implements Mapper {

    /**
     * Method writes null, for write uses classes that implement {@link JsonWriter}
     * @param object - null for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Object object, JsonWriter jsonWriter) {
        jsonWriter.writeNull();
    }
}
