package com.json.mapper;

import com.json.serializer.IndentedJsonSerializer;
import com.json.writer.JsonWriter;

import java.lang.reflect.Array;
/**
 * The class writes instance of primitive or object array to JSON.
 */
public class ArrayMapper implements Mapper {

    private IndentedJsonSerializer serializer;

    /**
     * Constructor initialize parameter {@link #serializer}
     * @param serializer - defines way to serialize child objects.
     */
    public ArrayMapper(IndentedJsonSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Method writes the object as an array, for write uses classes that implement {@link JsonWriter}
     * Write in sign "[...]", line of elements writes with comma
     * @param object - object for writing to writer
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Object object, JsonWriter jsonWriter) {
        jsonWriter.writeArrayBegin();
        int size = Array.getLength(object);
        for (int i = 0; i < size; i++) {
            serializer.serialize(Array.get(object, i), jsonWriter);
            if (i != size - 1) jsonWriter.writeSeparator();
        }
        jsonWriter.writeArrayEnd();
    }
}
