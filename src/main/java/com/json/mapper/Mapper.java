package com.json.mapper;

import com.json.writer.JsonWriter;

/**
 * Defines mappers for serialization different objects.
 */
public interface Mapper<T> {

    /**
     * @param t - object for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    void write (T t, JsonWriter jsonWriter);
}
