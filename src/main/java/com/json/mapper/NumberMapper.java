package com.json.mapper;

import com.json.writer.JsonWriter;

/**
 * The class writes {@link Number} to JSON.
 */
public class NumberMapper implements Mapper<Number> {

    /**
     * Method writes {@link Number}, for write uses classes that implement {@link JsonWriter}
     * @param number - {@link Number} for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Number number, JsonWriter jsonWriter) {
       jsonWriter.writeNumber(number);
    }
}
