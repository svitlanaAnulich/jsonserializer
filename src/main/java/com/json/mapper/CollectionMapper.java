package com.json.mapper;

import com.json.serializer.IndentedJsonSerializer;
import com.json.writer.JsonWriter;

import java.util.Collection;
import java.util.Iterator;

/**
 * The class writes instances of {@link Collection} as an array.
 */
public class CollectionMapper implements Mapper<Collection> {

    private IndentedJsonSerializer serializer;

    /**
     * Constructor contains parameter {@link #serializer}
     * @param serializer - defines way to serialize child objects
     */
    public CollectionMapper(IndentedJsonSerializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Method writes instances of {@link Collection} as an array,
     * for write uses classes that implement {@link JsonWriter}
     * Write in sign "[...]", line of elements writes with comma
     * @param object - instance of {@link Collection} for writing to JSON.
     * @param jsonWriter - defines rules for writing to JSON.
     */
    @Override
    public void write(Collection object, JsonWriter jsonWriter) {
        jsonWriter.writeArrayBegin();
        Iterator iterator = object.iterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            serializer.serialize(o, jsonWriter);
            if (iterator.hasNext()) jsonWriter.writeSeparator();
        }
        jsonWriter.writeArrayEnd();
    }
}
