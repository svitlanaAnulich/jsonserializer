package com.json.serializer;


import com.json.writer.JsonWriter;

import java.io.OutputStream;
import java.io.Writer;
import java.nio.charset.Charset;

/**
 * This interface is designed to convert objects into JSON.
 *
 */
public interface JsonSerializer {

    /**
     * Method called to check whether converted objects JSON will be with indents or no.
     * @return current indent
     */
    boolean isIndent();

    /**
     * Set intend.
     * @param indent is used to determine JSON with indents when serializing
     */
    void setIndent(boolean indent);

    /**
     * Method serializes given object.
     * @param obj object for serialization
     * @return String of object that has been serialized
     */
    String serialize(Object obj);

    /**
     * Method serializes given object to stream.
     * @param obj object for serialization
     * @param stream output stream for writing JSON into it
     */
    void serialize(Object obj, OutputStream stream);

    /**
     * Method serializes object given object to stream using {@link Charset}.
     * @param obj  object for serialization
     * @param stream  output stream for writing JSON into it
     * @param charset used for defining character encoding
     */
    void serialize(Object obj, OutputStream stream, Charset charset);

    /**
     * Method serializes given object and create {@link JsonWriter} accordingly to {@link #isIndent()}.
     * @param obj - object for serialization
     * @param writer - used for writing JSON into it
     */
    void serialize(Object obj, Writer writer);

}
