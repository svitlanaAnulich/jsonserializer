package com.json.serializer;

import com.json.mapper.*;
import com.json.writer.IndentedJsonWriter;
import com.json.writer.JsonWriter;
import com.json.writer.JsonWriterImpl;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is designed to convert objects into JSON, implements {@link JsonSerializer}.
 *
 * {@link #DEFAULT_CHARSET} - used for defining default character encoding for {@link #serialize(Object, OutputStream)}.
 * {@link #indent} - used to define JSON with indents
 * {@link #mappersCache} - contain standard mappers for serializing different objects
 */
public class JsonSerializerImpl implements JsonSerializer{

    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
    private Map<Class, Mapper<?>> mappersCache;
    private boolean indent;

    /**
     * Constructor adds to {@link #mappersCache} standard mappers.
     */
    public JsonSerializerImpl() {
        mappersCache = new HashMap<>();
        mappersCache.put(String.class, new StringMapper());
        mappersCache.put(Boolean.class, new BooleanMapper());
        mappersCache.put(Number.class, new NumberMapper());
        mappersCache.put(null, new NullMapper());
        mappersCache.put(Object[].class, new ArrayMapper(this::serialize));
        mappersCache.put(Map.class, new MapMapper(this::serialize));
        mappersCache.put(Collection.class, new CollectionMapper(this::serialize));
        mappersCache.put(Object.class, new ObjectMapper(this::serialize));
        mappersCache.put(Character.class, new CharMapper());
    }

    /**
     * Method called to check whether converted objects JSON will be with indents or no.
     * @return current indent
     */
    @Override
    public boolean isIndent() {
        return indent;
    }

    /**
     * Set intend.
     * @param indent is used to determine JSON with indents when serializing
     */
    @Override
    public void setIndent(boolean indent) {
        this.indent = indent;
    }

    /**
     * Method serializes given object.
     * @param object object for serialization
     * @return String of object that has been serialized
     */
    @Override
    public String serialize(Object object) {
        StringWriter writer = new StringWriter();
        serialize(object, writer);
        return writer.toString();

    }

    /**
     * Method serializes given object to stream.
     * It uses default charset {@link #DEFAULT_CHARSET}.
     * @param object object for serialization
     * @param stream output stream for writing JSON into it
     */
    @Override
    public void serialize(Object object, OutputStream stream) {
        serialize(object, stream, DEFAULT_CHARSET);
    }

    /**
     * Method serializes object given object to stream using {@link Charset}.
     * @param object  object for serialization
     * @param stream  output stream for writing JSON into it
     * @param charset used for defining character encoding
     */
    @Override
    public void serialize(Object object, OutputStream stream, Charset charset) {
        serialize(object, new OutputStreamWriter(stream, charset));
    }

    /**
     * Method serializes given object and create {@link JsonWriter} accordingly to {@link #isIndent()}.
     * @param object - object for serialization
     * @param writer - used for writing JSON into it
     */
    @Override
    public void serialize(Object object, Writer writer) {
        if (isIndent()) {
            serialize(object, new IndentedJsonWriter(writer));
        } else {
            serialize(object, new JsonWriterImpl(writer));
        }
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Mappers that contain child elements and there constructor require {@link IndentedJsonSerializer}
     * retrieve mapper and writes object
     * @param object - object for serialization
     * @param writer - defines rules for writing to JSON
     */
    public void serialize(Object object, JsonWriter writer) {
        Mapper mapper = getMapper(object);
        mapper.write(object, writer);
    }

    /**
     * Get mapper from the {@link #mappersCache}
     * @param object - object for serialization
     * @return - mapper
     */
    private Mapper getMapper(Object object) {
        if (object == null) {
            return mappersCache.get(null);
        }
        Class clazz = object.getClass();
        if (clazz.isArray()){
            return mappersCache.get(Object[].class);
        } else if (mappersCache.containsKey(clazz)) {
            return mappersCache.get(clazz);
        } else if (object instanceof Map) {
            return mappersCache.get(Map.class);
        } else if (object instanceof Collection) {
            return mappersCache.get(Collection.class);
        } else if (object instanceof Number){
            return mappersCache.get(Number.class);
        } else if (object instanceof String){
            return mappersCache.get(String.class);
        } else if (object instanceof  Boolean){
            return mappersCache.get(Boolean.class);
        } else {
            return mappersCache.get(Object.class);
        }

    }
}
