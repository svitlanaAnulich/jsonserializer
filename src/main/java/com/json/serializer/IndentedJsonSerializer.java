package com.json.serializer;

import com.json.writer.JsonWriter;

/**
 * Used for encapsulation serialization of child elements in {@link com.json.mapper.Mapper}
 */
public interface IndentedJsonSerializer {

    /**
     * Writes given object to writer
     * @param obj - child element for serialization
     * @param jsonWriter - defines rules for writing to JSON
     */
    void serialize(Object obj, JsonWriter jsonWriter);
}
