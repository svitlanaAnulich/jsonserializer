package com.json.writer;

/**
 * Class JsonWriter determines the rules for writing objects
 */
public interface JsonWriter {

    /**
     * Method writes a sign "{" in the begin of object, example: {(object)
     */
    void writeObjectBegin();

    /**
     * Method writes a sign "}" in the end of object, example: (object)}
     */
    void writeObjectEnd();

    /**
     * Method writes a sign "[" in the begin of array, example: [(array)
     */
    void writeArrayBegin();

    /**
     * Method writes a sign "]" in the end of array, example: (array)]
     */
    void writeArrayEnd();

    /**
     * Method writes String value in double quotes, example: "any text"
     * @param str - taken in String value
     */
    void writeString(String str);

    /**
     * Method write number as a text
     * @param number - a number
     */
    void writeNumber(Number number);

    /**
     * Method writes comma
     */
    void writeSeparator();

    /**
     * Method writes colon
     */
    void writePropertySeparator();

    /**
     * Method writes boolean value as a text
     */
    void writeBoolean(Boolean value);

    /**
     * Method writes char value in double quotes, example: "a"
     * @param value - taken in a char value
     */
    void writeChar(char value);

    /**
     * Method writes null as a text
     */
    void writeNull();

}
