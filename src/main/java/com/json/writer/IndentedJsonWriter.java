package com.json.writer;

import java.io.IOException;
import java.io.Writer;

/**
 * Class overrides the methods of {@link JsonWriterImpl} and adds the symbols of white space,
 * new line to the methods
 * {@link #currentLevel} - the depth of indents, is used for determine indent's count
 * {@link #isPropertySeparator} - the sign colon changes this value to true,
 * is used for writing values without indent after colon.
 */
public class IndentedJsonWriter extends JsonWriterImpl {

    private int currentLevel = 0;
    private boolean isPropertySeparator = false;

    /**
     * Constructor calls constructor of the super class.
     * @param writer - the stream {@link Writer} to write JSON.
     */
    public IndentedJsonWriter(Writer writer) {
        super(writer);
    }

    /**
     * Method writes a sign "{" and if there is not previous colon, writes new line after "{" ;
     * also this method increments {@link #currentLevel}
     */
    public void writeObjectBegin() {
        writeAll(() -> super.writeObjectBegin());
        writeLine();
        currentLevel++;
    }

    /**
     * Method writes a sign "}" from the new line and decrements {@link #currentLevel}
     */
    public void writeObjectEnd() {
        currentLevel--;
        writeLine();
        writeSpaces();
        super.writeObjectEnd();
    }

    /**
     * Method writes a sign "[" and if there is not previous colon, writes new line after "[" ;
     * also this method increments {@link #currentLevel}
     */
    public void writeArrayBegin() {
        writeAll(() -> super.writeArrayBegin());
        writeLine();
        currentLevel++;
    }

    /**
     * Method writes a sign "]" from the new line and decrements {@link #currentLevel}
     */
    public void writeArrayEnd() {
        currentLevel--;
        writeLine();
        writeSpaces();
        super.writeArrayEnd();
    }

    /**
     * Method writes in double quotes taken value
     * @param str - String value and if there is not previous colon, writes indent before value
     */
    public void writeString(String str) {
        writeAll(() -> super.writeString(str));
    }

    /**
     * Method writes number
     * @param number - number and if there is not previous colon, writes indent before value
     */
    public void writeNumber(Number number) {
        writeAll(() -> super.writeNumber(number));
    }

    /**
     * Method writes boolean value as text
     * @param value - number and if there is not previous colon, writes indent before value
     */
    public void writeBoolean(Boolean value) {
        writeAll(() -> super.writeBoolean(value));
    }

    /**
     * Method writes null as text and if there is not previous colon,
     * writes indent before value
     */
    public void writeNull() {
        writeAll(() -> super.writeNull());
    }

    /**
     * Method writes in double quotes taken in value
     * @param value - character value and if there is not previous colon, writes indent before value
     */
    public void writeChar(char value) {
        writeAll(() -> super.writeChar(value));
    }

    /**
     * Method writes colon with white space after, changes the boolean
     * {@link #isPropertySeparator} to true
     */
    public void writePropertySeparator() {
        isPropertySeparator = true;
        try {
            writer.write(" ");
            super.writePropertySeparator();
            writer.write(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes comma and write new line after
     */
    public void writeSeparator() {
        super.writeSeparator();
        writeLine();
    }


    /**
     * Method includes the same part of code of previous methods and takes in the empty method of
     * {@link LambdaWriter} interface
     * This method changes {@link #isPropertySeparator} from true to false, or write indent
     * before calling the method of interface
     * @param lambdaWriter
     */
    private void writeAll(LambdaWriter lambdaWriter) {
        if (isPropertySeparator) {
            lambdaWriter.run();
            isPropertySeparator = false;
        } else {
            writeSpaces();
            lambdaWriter.run();
        }
    }

    /**
     * Write new line symbol
     */
    private void writeLine() {
        try {
            writer.write("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Write {@link #currentLevel} count spaces
     */
    private void writeSpaces() {
        for (int i = 0; i < currentLevel; i++) {
            try {
                writer.write("  ");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /*
     * Interface includes only empty method that give ability to use methods with values
     * and without its in lambda-sentences in previous methods for make code easier
     */
    @FunctionalInterface
    private interface LambdaWriter {
        void run();
    }
}
