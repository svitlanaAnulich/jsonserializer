package com.json.writer;

import java.io.IOException;
import java.io.Writer;

/**
 * Class JsonWriter determines the rules for writing objects,
 * implements the interface
 * @see JsonWriter
 */
public class JsonWriterImpl implements JsonWriter {

    protected Writer writer;

    /**
     * Constructor take in the next parameter:
     * @param writer - the stream {@link Writer} to write JSON
     */
    public JsonWriterImpl(Writer writer) {
        this.writer = writer;
    }

    /**
     * Method writes a sign "{" in the begin of object, example: {(object)
     */
    public void writeObjectBegin() {
        try {
            writer.write("{");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "}" in the end of object, example: (object)}
     */
    public void writeObjectEnd() {
        try {
           writer.write("}");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "[" in the begin of array, example: [(array)
     */
    public void writeArrayBegin() {
        try {
            writer.write("[");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes a sign "]" in the end of array, example: (array)]
     */
    public void writeArrayEnd() {
        try {
            writer.write("]");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes String value in double quotes, example: "any text"
     * @param str - taken in String value
     */
    public void writeString(String str) {
        try {
            writer.write("\"" + str + "\"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method write number as a text
     * @param number - a number
     */
    public void writeNumber(Number number) {
        try {
            writer.write(String.valueOf(number));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes comma
     */
    public void writeSeparator() {
        try {
            writer.write(",");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes colon
     */
    public void writePropertySeparator() {
        try {
            writer.write(":");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes boolean value as a text
     */
    public void writeBoolean(Boolean value) {
        try {
            writer.write(String.valueOf(value));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes null as a text
     */
    public void writeNull() {
        try {
            writer.write("null");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method writes char value in double quotes, example: "a"
     * @param value - taken in a char value
     */
    public void writeChar(char value) {
        try {
            writer.write("\"" + value + "\"");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
