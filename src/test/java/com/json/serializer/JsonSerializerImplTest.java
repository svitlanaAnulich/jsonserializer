package com.json.serializer;


import com.json.exmapclass.FullParametersFields;
import com.json.exmapclass.FullParametersFieldsSupper;
import com.json.exmapclass.ObjectMock;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import static org.junit.Assert.*;

public class JsonSerializerImplTest {

    private JsonSerializer serializer = new JsonSerializerImpl();

    @Test
    public void serializeArrayPrimitiveType(){
        int[] nums = {1, 2, 3, 4};
        String expected = "[1,2,3,4]";

        assertEquals(expected, serializer.serialize(nums));
    }

    @Test
    public void serializeArrayObject(){
        String[] array = {"one", "two", "three", "four"};
        String expected = "[\"one\",\"two\",\"three\",\"four\"]";

        assertEquals(expected, serializer.serialize(array));
    }

    @Test
    public void serializeArrayNull(){
        String[] array = null;
        String expected = "null";

        assertEquals(expected, serializer.serialize(array));
    }

    @Test
    public void serializeObject() {
        ObjectMock objectMock = new ObjectMock();
        assertEquals(objectMock.toJsonString(),serializer.serialize(objectMock));
    }

    @Test
    public void serializeObjectOutputStream() {
        ObjectMock objectMock = new ObjectMock();
        OutputStream outputStream = new ByteArrayOutputStream();
        serializer.serialize(objectMock, outputStream);
        String actual = outputStream.toString();
        assertEquals(objectMock.toJsonString(), actual);
    }

    @Test
    public void serializeHardObject(){
        FullParametersFields fullParametersFields = new FullParametersFields();
        assertEquals(fullParametersFields.toJsonString(), serializer.serialize(fullParametersFields));
    }




}
