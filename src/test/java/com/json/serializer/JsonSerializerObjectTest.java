package com.json.serializer;


import com.json.exmapclass.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class JsonSerializerObjectTest {

    private JsonSerializer serializer = new JsonSerializerImpl();

    @Test
    public void writeIgnorePrivate() {
        PrivateFields privateFields = new PrivateFields();
        assertEquals(privateFields.toJsonString(),serializer.serialize(privateFields));
    }

    @Test
    public void writeJsonIgnoreAnnotation() {
        JsonIgnoreFields jsonIgnoreFields = new JsonIgnoreFields();
        assertEquals(jsonIgnoreFields.toJsonString(), serializer.serialize(jsonIgnoreFields));
    }

    @Test
    public void writeIgnoreTransientField() {
        TransientField transientField = new TransientField();
        assertEquals(transientField.toJsonString(), serializer.serialize(transientField));
    }

    @Test
    public void writeProtectedFields(){
        ProtectedField protectedField = new ProtectedField();
        assertEquals(protectedField.toJsonString(), serializer.serialize(protectedField));
    }

    @Test
    public void writeJsonPropertyAnnotation(){
        JsonPropertyFields jsonPropertyFields = new JsonPropertyFields();
        assertEquals(jsonPropertyFields.toJsonString(), serializer.serialize(jsonPropertyFields));
    }

    @Test
    public void writeJsonSeizedNameFields(){
        JsonSerializedNameFields jsonSerializedNameFields = new JsonSerializedNameFields();
        assertEquals(jsonSerializedNameFields.toJsonString(), serializer.serialize(jsonSerializedNameFields));
    }

    @Test
    public void writeIncludingSupper(){
        FieldsIncludingSupper fieldsIncludingSupper = new FieldsIncludingSupper();
        assertEquals(fieldsIncludingSupper.toJsonString(), serializer.serialize(fieldsIncludingSupper));
    }
}
