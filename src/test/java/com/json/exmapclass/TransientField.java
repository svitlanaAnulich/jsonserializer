package com.json.exmapclass;

public class TransientField {

    public transient int id;
    public String string;
    public boolean is;
    public transient float aFloat;

    public TransientField() {
        this.id = 0;
        this.string = "test string";
        this.is = false;
        this.aFloat = 4.4f;
    }

    public String toJsonString(){
        return "{\"string\":\"test string\",\"is\":false}";
    }
}
