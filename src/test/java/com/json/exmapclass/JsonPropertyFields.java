package com.json.exmapclass;

import com.json.mapper.annotation.JsonProperty;

public class JsonPropertyFields {

    private int id;

    @JsonProperty
    private String string;

    @JsonProperty
    public transient boolean is;

    private float aFloat;

    public JsonPropertyFields() {
        this.id = 0;
        this.string = "test string";
        this.is = false;
        this.aFloat = 4.4f;
    }

    public String toJsonString(){
        return "{\"string\":\"test string\",\"is\":false}";
    }
}
