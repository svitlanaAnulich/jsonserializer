package com.json.exmapclass;

import com.json.mapper.annotation.JsonChangeSerializeName;
import com.json.mapper.annotation.JsonIgnore;
import com.json.mapper.annotation.JsonProperty;

public class FullParametersFields extends FullParametersFieldsSupper {

    public String string;

    @JsonChangeSerializeName(name = "name_boolean")
    public boolean aBoolean;

    @JsonProperty
    private int jsonProperty;

    @JsonIgnore
    public String jsonIgnore;

    public transient String transientField;

    public FullParametersFields() {
        this.string = "NEW TEST";
        aBoolean = true;
        jsonProperty = 30;
        jsonIgnore = "ignore";
        transientField = "transient Field";
    }

    @Override
    public String toJsonString(){

        String string = super.toJsonString();

        return "{\"string\":\"NEW TEST\"," +
                "\"name_boolean\":true," +
                "\"jsonProperty\":30," +
                string.substring(1,string.indexOf("transientField")-2)+"}";
    }


}
