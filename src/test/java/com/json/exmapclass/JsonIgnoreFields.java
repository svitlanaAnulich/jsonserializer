package com.json.exmapclass;

import com.json.mapper.annotation.JsonIgnore;

public class JsonIgnoreFields {

    @JsonIgnore
    public int id;
    public String string;
    public boolean is;
    @JsonIgnore
    public float aFloat;

    public JsonIgnoreFields() {
        this.id = 0;
        this.string = "test string";
        this.is = false;
        this.aFloat = 4.4f;
    }

    public String toJsonString(){
        return "{\"string\":\"test string\",\"is\":false}";
    }
}
