package com.json.exmapclass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ObjectMock {
    public char c = 'a';
    public boolean isMock = true;
    public List<String> list = new ArrayList<>();
    public Map<String, String> map = new LinkedHashMap<>();
    private boolean isWrite = true;

    public ObjectMock() {
        list.add("value1");
        list.add(null);

        map.put("key1", "value1");
        map.put("key2", "value2");
    }

    public String toJsonString(){
        return  "{\"c\":\"a\",\"isMock\":true,\"list\":[\"value1\",null]," +
                "\"map\":{\"key1\":\"value1\",\"key2\":\"value2\"}}";
    }
}
