package com.json.exmapclass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FullParametersFieldsSupper {

    public char c;
    public boolean isMock;
    public List<String> list;
    public Map<String, String> map;
    public int[] array;
    public Float aFloat;
    public PrivateFields privateFields;
    public String transientField;
    public String string;


    public FullParametersFieldsSupper() {
        c = 'a';
        isMock = true;

        list = new ArrayList<>();
        list.add("value1");
        list.add(null);

        map = new LinkedHashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");

        array = new int[]{2, 4, 5, 10};
        aFloat = 4.7f;

        privateFields = new PrivateFields();
        string = "TEST";
        transientField = "transient";

    }

    public String toJsonString() {
        return "{\"c\":\"a\"," +
                "\"isMock\":true," +
                "\"list\":[\"value1\",null]," +
                "\"map\":{\"key1\":\"value1\",\"key2\":\"value2\"}," +
                "\"array\":[2,4,5,10]," +
                "\"aFloat\":4.7," +
                "\"privateFields\":" + privateFields.toJsonString() + "," +
                "\"transientField\":transient" +
                "\"string\":\"TEST\"}";
    }


}
