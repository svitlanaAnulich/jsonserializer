package com.json.exmapclass;

public class FieldsIncludingSupper extends ProtectedField {

    public int id;

    public FieldsIncludingSupper() {
        this.id = 10;
    }

    @Override
    public String toJsonString(){
       return "{\"id\":10,"+super.toJsonString().substring(1);
    }
}
