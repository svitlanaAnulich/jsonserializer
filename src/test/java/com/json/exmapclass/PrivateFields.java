package com.json.exmapclass;

public class PrivateFields {

    private int id;
    public String string;
    public boolean is;
    private float aFloat;

    public PrivateFields() {
        this.id = 0;
        this.string = "test string";
        this.is = false;
        this.aFloat = 4.4f;
    }

    public String toJsonString(){
        return "{\"string\":\"test string\",\"is\":false}";
    }
}
