package com.json.exmapclass;

import com.json.mapper.annotation.JsonChangeSerializeName;

public class JsonSerializedNameFields {

    private int id;

    @JsonChangeSerializeName(name = "TEST_STRING_NAME")
    public String string;

    public boolean is;
    private float aFloat;

    public JsonSerializedNameFields() {
        this.id = 0;
        this.string = "test string";
        this.is = false;
        this.aFloat = 4.4f;
    }

    public String toJsonString(){
        return "{\"TEST_STRING_NAME\":\"test string\",\"is\":false}";
    }

}
