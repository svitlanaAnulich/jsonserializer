package com.json.exmapclass;

public class ProtectedField {

    protected String string;
    protected boolean is;

    public ProtectedField() {
        this.string = "test string";
        this.is = false;
    }

    public String toJsonString(){
        return "{\"string\":\"test string\",\"is\":false}";
    }
}
