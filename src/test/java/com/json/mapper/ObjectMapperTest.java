package com.json.mapper;

import com.json.exmapclass.*;
import com.json.serializer.IndentedJsonSerializer;
import com.json.writer.JsonWriter;
import org.junit.Before;
import org.junit.Test;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ObjectMapperTest {

    private Mapper mapper;
    private JsonWriter jsonWriter;
    private IndentedJsonSerializer serializer;

    @Before
    public void setUp() {
        jsonWriter = mock(JsonWriter.class);
        serializer = mock(IndentedJsonSerializer.class);
        mapper = new ObjectMapper(serializer);
    }

    @Test
    public void writeIgnorePrivateFields() {
        writeTemplate(new PrivateFields());
    }

    @Test
    public void writeJsonIgnoreAnnotation() {
        writeTemplate(new JsonIgnoreFields());
    }

    @Test
    public void writeIgnoreTransientFields(){
        writeTemplate(new TransientField());
    }

    @Test
    public void writeProtectedFields(){
        writeTemplate(new ProtectedField());
    }

    @Test
    public void writeJsonPropertyAnnotation(){
        writeTemplate(new JsonPropertyFields());
    }

    @Test
    public void writeJsonSeizedNameFields(){
        writeTemplate(new JsonSerializedNameFields(), "TEST_STRING_NAME");
    }


    private <T> void writeTemplate(T t){
        writeTemplate(t, "string");
    }

    private <T> void writeTemplate(T t, String strName){
        mapper.write(t, jsonWriter);

        verify(jsonWriter).writeObjectBegin();
        verify(jsonWriter).writeString(strName);
        verify(jsonWriter,times(2)).writePropertySeparator();
        verify(serializer).serialize("test string", jsonWriter);
        verify(jsonWriter).writeSeparator();
        verify(jsonWriter).writeString("is");
        verify(serializer).serialize(false, jsonWriter);
        verify(jsonWriter).writeObjectEnd();
    }

}
